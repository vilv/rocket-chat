import 'dart:async';
import 'dart:convert';

import 'package:base_getx/base_getx.dart';
import 'package:example/demo.dart';
import 'package:example/login/login_page.dart';
import 'package:example/message/web_socket.dart';
import 'package:example/noti.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

final String serverUrl = "http://10.50.48.72:3000";
final String webSocketUrl = "ws://10.50.48.72:3000/websocket";
// final String username = "demo1";
final String password = "123456";
String tokenRocket = '';
String userIdRocket = '';
String userName = '';
// final Channel channel = Channel(id: "zSTkkSFBjaACBTvN2");
// final Room room = Room(id: "zSTkkSFBjaACBTvN2");

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Rocket Chat Demo",
      home: LoginPage(),
    );
  }
}

