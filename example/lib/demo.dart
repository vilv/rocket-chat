
import 'package:flutter/material.dart';

import 'package:webview_flutter/webview_flutter.dart';


class WebLiveChat extends StatefulWidget {
  WebLiveChat({Key? key}) : super(key: key);

  @override
  State<WebLiveChat> createState() => _WebLiveChatState();
}

class _WebLiveChatState extends State<WebLiveChat> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Live chat"),
      ),
      body: WebView(
        // initialUrl: 'https://translate.google.com/?hl=vi',
        initialUrl: 'http://10.50.48.72:3000/livechat',
        onPageFinished: (url) {
          print(url);
        },
        javascriptMode: JavascriptMode.unrestricted,
      ),
    );
  }
}
