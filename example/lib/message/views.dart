import 'dart:io';
import 'package:example/base/image_network.dart';
import 'package:rocket_chat_connector_flutter/models/model_export.dart';
import 'package:flutter/material.dart';
import 'package:mime/mime.dart';

class BuildMessageMedia extends StatelessWidget {
  final FileMessage fileMessage;
  final String nameSend;
  final bool isUser;
  const BuildMessageMedia(
      {Key? key,
      required this.fileMessage,
      required this.isUser,
      required this.nameSend})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget file;
    switch (fileMessage.typeFile) {
      case TypeFile.image:
        file = ImageNetWork(
          url: fileMessage.url,
          width: fileMessage.image!.width,
          height: fileMessage.image!.height,
          fit: BoxFit.cover,
        );
        break;
      case TypeFile.application:
        file = TextButton(onPressed: () {}, child: Text(fileMessage.name));
        break;
      default:
        file = Text(fileMessage.name);
    }
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        crossAxisAlignment:
            isUser ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        mainAxisAlignment:
            isUser ? MainAxisAlignment.end : MainAxisAlignment.start,
        children: [
          Visibility(
            visible: !isUser,
            child: Text(
              nameSend,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5),
            child: file,
          ),
          Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
                color: !isUser ? Colors.orange[200] : Colors.grey[200],
                borderRadius: BorderRadius.circular(10)),
            child: Text(
              fileMessage.description,
            ),
          ),
        ],
      ),
    );
  }
}

class BuildFilePreview extends StatelessWidget {
  final String path;
  const BuildFilePreview({Key? key, required this.path}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final mimeType = lookupMimeType(path);
    if (mimeType!.contains('image')) {
      return Image.file(
        File(path),
        height: 100,
        width: 200,
        fit: BoxFit.cover,
      );
    } else {
      return Container(
        height: 40,
        color: Colors.grey,
        child: Row(
          children: [
            Icon(Icons.file_upload),
            SizedBox(
              width: 10,
            ),
            Text(path),
          ],
        ),
      );
    }
  }
}
