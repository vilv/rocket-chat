import 'dart:convert';

import 'package:base_getx/base_getx.dart';
import 'package:example/base/base_getview.dart';
import 'package:example/base/dash_chat.dart';
import 'package:example/main.dart';
import 'package:example/message/message_controller.dart';
import 'package:example/message/views.dart';
import 'package:rocket_chat_connector_flutter/models/model_export.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class MessagePage extends BaseGetWidget<MessageController> {
  @override
  MessageController initController() {
    return Get.put(MessageController());
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MessageController>(
        init: initController(),
        builder: (c) {
          return Scaffold(
            appBar: AppBar(
              title: Text(controller.roomName),
            ),
            body: Column(
              children: [
                Expanded(
                  child: StreamBuilder(
                      stream: controller.webSocket.webSocketChannel.stream,
                      builder: (context, snap) {
                        print(snap.data);
                        if (snap.hasData) {
                          handleDataReceive(snap);
                          handleCreateDirectMessage(snap);
                        }
                        int sizeList = controller.messages.length - 1;
                        return SmartRefresher(
                          enablePullUp: true,
                          enablePullDown: false,
                          controller: controller.refreshController,
                          onLoading: () {
                            controller.getMessage(isLoadMore: true);
                          },
                          child: ListView.separated(
                            controller: controller.scrollController,
                            itemCount: sizeList + 1,
                            shrinkWrap: true,
                            reverse: true,
                            separatorBuilder: (context, index) {
                              return SizedBox(
                                height: 20,
                              );
                            },
                            itemBuilder: (context, index) {
                              if (controller.messages
                                      .elementAt(sizeList - index)
                                      .fileMessage !=
                                  null) {
                                return BuildMessageMedia(
                                  fileMessage: controller.messages
                                      .elementAt(sizeList - index)
                                      .fileMessage!,
                                  isUser: userName ==
                                      controller.messages
                                          .elementAt(sizeList - index)
                                          .user
                                          .username,
                                  nameSend: controller.messages
                                      .elementAt(sizeList - index)
                                      .user
                                      .name,
                                );
                              }
                              return DashChat(
                                message: controller.messages
                                    .elementAt(sizeList - index),
                              );
                            },
                          ),
                        );
                      }),
                ),
                SizedBox(
                  height: 20,
                ),
                _buildInputText()
              ],
            ),
          );
        });
  }

  Widget _buildInputText() {
    return Container(
      decoration: BoxDecoration(color: Colors.grey[200]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          controller.currentFilePath != null
              ? Stack(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child:
                          BuildFilePreview(path: controller.currentFilePath!),
                    ),
                    Positioned(
                        right: 0,
                        top: 0,
                        child: IconButton(
                            onPressed: () {
                              controller.currentFilePath = null;
                              controller.update();
                            },
                            icon: Icon(
                              Icons.close,
                              color: Colors.white,
                            )))
                  ],
                )
              : Container(),
          TextFormField(
            controller: controller.textController,
            onFieldSubmitted: (value) {
              handleSendMessage();
            },
            textInputAction: TextInputAction.send,
            decoration: InputDecoration(
              hintText: 'Nhập nội dung...',
              prefixIcon: IconButton(
                  onPressed: () {
                    showPickFile();
                  },
                  icon: Icon(Icons.add)),
              fillColor: Colors.white,
              contentPadding: EdgeInsets.symmetric(horizontal: 10),
              filled: true,
              suffixIcon: IconButton(
                  onPressed: () {
                    handleSendMessage();
                  },
                  icon: Icon(Icons.send)),
              border: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.circular(10)),
            ),
          ),
        ],
      ),
    );
  }

  void handleSendMessage() {
    if (controller.textController.text.trim().isNotEmpty ||
        controller.currentFilePath != null) {
      if (controller.currentFilePath != null) {
        controller.sendFileToRoomChat(controller.textController.text);
      } else {
        controller.webSocket.sendMessageOnChannel(
            controller.textController.text, controller.roomId);
      }
      controller.textController.clear();
      controller.scrollController.animateTo(
          controller.scrollController.position.minScrollExtent,
          duration: Duration(milliseconds: 300),
          curve: Curves.linear);
    }
  }

  void handleDataReceive(AsyncSnapshot snapshot) {
    var data = snapshot.data.toString();
    final result = jsonDecode(data);
    if (result['fields'] != null) {
      MessageChat message = MessageChat.fromSnapShot(result);
      controller.messages.add(message);
      controller.scrollController.animateTo(
          controller.scrollController.position.minScrollExtent,
          duration: Duration(milliseconds: 300),
          curve: Curves.linear);
    }
  }

  void handleCreateDirectMessage(AsyncSnapshot snapshot) {
    var data = snapshot.data.toString();
    final result = jsonDecode(data);
    if (result['result'] != null) {
      print(
          'Create direct message success with roomId = ${result['result']['rid']}');
      controller.roomId = result['result']['rid'];
    }
  }

  void showPickFile() {
    showModalBottomSheet(
      context: Get.context!,
      builder: (context) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 15),
              child: Text(
                'Chọn ảnh để tải lên',
              ),
            ),
            Divider(
              height: 1,
            ),
            ListTile(
              onTap: () async {
                final path = await controller.pickFile.pickImageFromGalery();
                if (path != null) {
                  controller.currentFilePath = path;
                  Get.back();
                  controller.update();
                }
              },
              leading: Icon(
                Icons.image,
                color: Colors.blue,
              ),
              title: Text(
                'Chọn ảnh từ thư viện',
              ),
            ),
            Divider(
              height: 1,
            ),
            ListTile(
              onTap: () async {
                final path = await controller.pickFile.pickImageFromCamera();
                if (path != null) {
                  controller.currentFilePath = path;
                  Get.back();
                  controller.update();
                }
              },
              leading: Icon(
                Icons.camera,
              ),
              title: Text(
                'Chụp một bức ảnh mới',
              ),
            ),
            Divider(
              height: 1,
            ),
            ListTile(
              onTap: () async {
                final path = await controller.pickFile.pickFileFromStorage();
                if (path != null) {
                  controller.currentFilePath = path;
                  Get.back();
                  controller.update();
                }
              },
              leading: Icon(Icons.file_present),
              title: Text(
                'Chọn file',
              ),
            ),
          ],
        );
      },
    );
  }
}
