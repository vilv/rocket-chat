import 'package:base_getx/base_getx.dart';
import 'package:example/chat/chat_repository.dart';
import 'package:rocket_chat_connector_flutter/models/model_export.dart';
import 'package:rocket_chat_connector_flutter/services/rocket_websocket.dart';
import 'package:example/utils/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../main.dart';

class MessageController extends BaseGetXController {
  late String roomId;
  late String roomName;
  int pageIndex = 0;
  RocketWebSocket webSocket = RocketWebSocket(
      mServerUrl: serverUrl,
      mWebSocketUrl: webSocketUrl);
  final pickFile = PickerFile();
  final textController = TextEditingController();
  // RxList<MessageChat> messages = RxList<MessageChat>([]);
  List<MessageChat> messages = [];
  final scrollController = ScrollController();
  final refreshController = RefreshController();
  bool isChannel = false;
  String? currentFilePath;
  @override
  void onInit() {
    var data = Get.arguments;
    roomId = data['roomId'] ?? '';
    roomName = data['roomName'];
    String? username = data['username'];
    isChannel = data['isChannel'] ?? false;
    webSocket.initSocketChannel(tokenRocket: tokenRocket);
    if (username != null) {
      // tạo một cuộc hội thoại giữa 2 user
      webSocket.createDirectMessage(username);
    } else {
      webSocket.streamChannelMessagesSubscribe(roomId);
      // get 10 tin nhắn gần nhất
      getMessage();
    }

    super.onInit();
  }

  @override
  void onClose() {
    webSocket.closeWebSocket();
    super.onClose();
  }

  final _repository = ChatRepository();
  void getMessage({bool isLoadMore = false}) async {
    print('get message wiht pageIndex = $pageIndex');
    var res = await _repository.getMessages(roomId, pageIndex, isChannel);
    if (res.isNotEmpty) {
      pageIndex++;
      refreshController.loadComplete();
    } else {
      refreshController.loadNoData();
    }
    if (isLoadMore) {
      messages.insertAll(0, res);
    } else {
      messages = res.reversed.toList();
    }
    update();
    // update();
  }

  void sendFileToRoomChat(String describe) async {
    print('send file to room with id = $roomId');
    var res = await _repository.uploadFile(roomId, currentFilePath!, describe);
    if (res) {
      currentFilePath = null;
      update();
    } else {
      Get.showSnackbar(GetSnackBar(
        message: 'Có lỗi xảy ra!',
      ));
    }
  }
}
