// import 'dart:convert';
//
// import 'package:example/base/services.dart';
// import 'package:example/main.dart';
// import 'package:web_socket_channel/io.dart';
// import 'package:web_socket_channel/web_socket_channel.dart';
//
// class WebSocket {
//   static final instance = WebSocket._internal();
//   WebSocket._internal();
//   late WebSocketChannel webSocketChannel;
//   void initSocketChannel() {
//     webSocketChannel = IOWebSocketChannel.connect(RocketService.webSocketUrl);
//     webSocketChannel.sink.add(jsonEncode({
//       "msg": "connect",
//       "version": "1",
//       "support": ["1", "pre2", "pre1"]
//     }));
//     webSocketChannel.sink.add(jsonEncode({
//       "msg": "method",
//       "method": "login",
//       "id": "42",
//       "params": [
//         {"resume": tokenRocket}
//       ]
//     }));
//   }
//
//   void streamChannelMessagesSubscribe(String channelId) {
//     webSocketChannel.sink.add(jsonEncode({
//       "msg": "sub",
//       "id": channelId,
//       "name": "stream-room-messages",
//       "params": [channelId, false]
//     }));
//   }
//
//   void streamNotifyUserSubscribe(String userId) {
//     webSocketChannel.sink.add(jsonEncode({
//       "msg": "sub",
//       "id": userId + "subscription-id",
//       "name": "stream-notify-user",
//       "params": [userId + "/notification", false]
//     }));
//   }
//   // void streamLiveChatRoom(String id) {
//   //   webSocketChannel.sink.add(jsonEncode({
//   //     "msg": "sub",
//   //     "id":id,
//   //     "name": "stream-room-messages",
//   //     "params": [userId + "/notification", false]
//   //   }));
//   // }
//
//   void sendMessageOnChannel(String message, String channelId) {
//     print("send msg = $message at channel $channelId");
//     webSocketChannel.sink.add(jsonEncode({
//       "msg": "method",
//       "method": "sendMessage",
//       "id": "42",
//       "params": [
//         {"rid": channelId, "msg": message}
//       ]
//     }));
//   }
//
//   void getInitialDataLiveChat() {
//     webSocketChannel.sink.add(jsonEncode({
//       "msg": "method",
//       "method": "livechat:getInitialData",
//       "params": [tokenRocket],
//       "id": "1"
//     }));
//   }
//
//   void sendMessageToLiveChat(String content, String id) {
//     print("send msg = $content by guest accout with id $id");
//     webSocketChannel.sink.add(jsonEncode({
//       "msg": "method",
//       "method": "sendMessageLivechat",
//       "id": "11",
//       "params": [
//         {
//           "_id": id,
//           "rid": id,
//           "msg": content,
//           "token": tokenRocket
//         }
//       ]
//     }));
//   }
//
//   void registerGuestAccount(String departmentId) {
//     webSocketChannel.sink.add(jsonEncode({
//       "msg": "method",
//       "method": "livechat:registerGuest",
//       "params": [
//         {
//           "token": tokenRocket,
//           "name": "Guest Name",
//           "email": "guest@rocket.chat",
//           "department": departmentId
//         }
//       ],
//       "id": "5"
//     }));
//   }
//
//   void getRooms() {
//     final json = {
//       "msg": "method",
//       "method": "rooms/get",
//       "id": "42",
//       "params": [
//         {"\$date": DateTime.now().millisecondsSinceEpoch}
//       ]
//     };
//     webSocketChannel.sink.add(jsonEncode(json));
//   }
//
//   void createDirectMessage(String userName) {
//     webSocketChannel.sink.add(jsonEncode({
//       "msg": "method",
//       "id": "42",
//       "method": "createDirectMessage",
//       "params": [userName]
//     }));
//   }
//
//   void closeWebSocket() {
//     webSocketChannel.sink.close();
//   }
// }
