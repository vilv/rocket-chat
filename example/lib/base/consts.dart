class RocketConst {
  static final String channelsUrl = 'api/v1/channels.list';
  static final String loginUrl = 'api/v1/login';
  static final String messageByChannelUrl = 'api/v1/channels.messages';
  static final String messageByUserUrl = 'api/v1/im.messages';
  static final String getRoomsUrl = 'api/v1/rooms.get';
  static final String uploadRoomsUrl = 'api/v1/rooms.upload';
  static final String getUserUrl = 'api/v1/users.list';
  static final String registerUrl = 'api/v1/users.register';
  static final String saveTokenNotiUrl = 'api/v1/push.token';
  static final String liveChatDepartmentUrl = 'api/v1/livechat/department';
}
