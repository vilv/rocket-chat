import 'package:example/main.dart';
import 'package:example/utils/convert.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:rocket_chat_connector_flutter/models/message.dart';

class DashChat extends StatelessWidget {
  final MessageChat message;
  const DashChat({
    Key? key,
    required this.message,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String dateTime = '';
    // dateTime = convertDateToString3(message.timeChat);
    bool isUser = userName == message.user.username;
    final now = DateTime.now();
    if (message.timeSend.isBefore(DateTime(now.year, now.month, now.day))) {
      dateTime = convertDateToString1(message.timeSend);
    } else {
      dateTime = convertDateToString3(message.timeSend);
    }
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        crossAxisAlignment:
            !isUser ? CrossAxisAlignment.start : CrossAxisAlignment.end,
        children: [
          !isUser
              ? Padding(
                  padding: EdgeInsets.only(bottom: 8),
                  child: Text(
                    message.user.name,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    // style: TextStyle()
                  ),
                )
              : Container(),
          Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
                color: !isUser ? Colors.orange[200] : Colors.grey[200],
                borderRadius: BorderRadius.circular(10)),
            child: Text(
              message.msg,
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Text(
            dateTime,
            style: TextStyle(fontSize: 12),
          )
        ],
      ),
    );
  }
}

class MediaMessage extends StatelessWidget {
  final FileMessage fileMessage;
  const MediaMessage({Key? key, required this.fileMessage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
