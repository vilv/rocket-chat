import 'dart:convert';

import 'package:base_getx/base_getx.dart';
import 'package:example/chat/chat_controller.dart';
import 'package:example/chat/chat_repository.dart';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:rocket_chat_connector_flutter/models/message.dart';
import 'package:rocket_chat_connector_flutter/models/rooms.dart';

class NotificationApp {
  static final FirebaseMessaging messaging = FirebaseMessaging.instance;
  static final localNotification = FlutterLocalNotificationsPlugin();
  static Future<void> initNotification(void Function() onClickNoti) async {
    var response = await messaging.requestPermission(
      alert: true,
      announcement: true,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );
    messaging.setAutoInitEnabled(true);
    messaging.setForegroundNotificationPresentationOptions(
        alert: true, badge: true, sound: true);
    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('@drawable/ic_launcher');
    final IOSInitializationSettings initializationSettingsIOS =
        IOSInitializationSettings();
    final InitializationSettings initializationSettings =
        InitializationSettings(
            android: initializationSettingsAndroid,
            iOS: initializationSettingsIOS);
    localNotification.initialize(
      initializationSettings,
      onSelectNotification: (payload) async {
        print('onselected');
        onClickNoti();
      },
    );
    if (response.authorizationStatus == AuthorizationStatus.authorized) {
      String firebaseToken = await messaging.getToken() ?? '';
      ChatRepository().saveTokenToServer(firebaseToken).then((value) {
        print(value);
      });
    } else if (response.authorizationStatus == AuthorizationStatus.denied) {}
    FirebaseMessaging.onMessage.listen((event) {
      print(event.data);
      Map json = jsonDecode(event.data['ejson']);
      UserSend userSend = UserSend.fromJson(json['sender']);
      String roomId = json['rid'];
      String content = '';
      String nameChannel = '';
      String dataMessage = event.data['message'];
      final message = dataMessage.split(':');
      if (message.length > 1) {
        content = message.last;
        nameChannel = json['name'];
      } else {
        content = dataMessage;
      }
      showNotification('Tin nhắn từ ${userSend.name}', content);
      final controller = Get.find<ChatController>();
      controller.setRoomNewMessage(Room(
          name: message.length > 1 ? nameChannel : userSend.name,
          id: roomId,
          userSend: userSend.name,
          isChannel: message.length > 1,
          lastMessage: content,
          countUnread: 1,
          userNameSend: userSend.username,
          timeSend: DateTime.now(),
          typeMessage: ''));
    });
    FirebaseMessaging.onMessageOpenedApp.listen((event) {
      print(event);
    });
  }

  static void showNotification(String title, String body) async {
    if (title.isNotEmpty && body.isNotEmpty) {
      var androidPlatformChannelSpecifics = AndroidNotificationDetails(
          'default_notification_channel_id', 'default_notification_channel',
          importance: Importance.max,
          priority: Priority.high,
          icon: 'ic_launcher',
          ticker: 'ticker',
          playSound: true);
      var iOSPlatformChannelSpecifics = IOSNotificationDetails();
      var platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics,
      );
      localNotification.show(0, title, body, platformChannelSpecifics);
    }
  }
}
