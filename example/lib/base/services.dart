import 'package:base_getx/base_getx.dart';
import 'package:dio/dio.dart';
import 'package:example/login/login_page.dart';
import 'package:example/main.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class RocketService extends BaseRequest {
  static Dio dio = Dio();
  static final _singleton = RocketService._internal();
  RocketService._internal();
  factory RocketService() {
    dio.options.connectTimeout = 30000;
    dio.options.baseUrl = serverUrl;
    // dio.interceptors.add(PrettyDioLogger(
    //     requestHeader: true,
    //     requestBody: true,
    //     responseBody: true,
    //     responseHeader: false,
    //     error: true,
    //     compact: true,
    //     maxWidth: 90)
    // );
    return _singleton;
  }
  static final String serverUrl = "http://10.50.48.72:3000/";
  static final String baseImageUrl = "http://10.50.48.72:3000";
  static final String webSocketUrl = "ws://10.50.48.72:3000/websocket";
  @override
  Future<Map<String, dynamic>> getBaseHeader() async {
    Map<String, String> map = {
      'X-Auth-Token': tokenRocket,
      'X-User-Id': userIdRocket,
    };
    return map;
  }

  @override
  void handleDioError(DioError dioError) {
    int statusCode = 0;
    String message = '';
    // var controller = Get.;
    if (dioError.response != null) {
      statusCode = dioError.response!.statusCode ?? 0;
      message = dioError.response!.statusMessage ?? 'Lỗi';
    }
    if (statusCode == 401) {
      ShowDialog().showDialogNotification(
          title: 'Thông báo',
          content: message,
          onClick: () {
            Get.offAll(LoginPage());
          });
    } else {
      ShowDialog().showDialogNotification(
        title: 'Thông báo',
        content: message,
      );
    }
  }

  @override
  Future<dynamic> sendRequest(
      {required String path,
      required RequestMethod requestMethod,
      body,
      param,
      bool sendHeader = true,
      bool isHandleError = true,
      void Function(int p1, int p2)? onProgress}) async {
    dynamic response;
    if (requestMethod == RequestMethod.GET) {
      response = await dio.get(
        path,
        queryParameters: param,
        onReceiveProgress: onProgress,
        options: Options(
            headers: sendHeader ? await getBaseHeader() : null,
            listFormat: ListFormat.multiCompatible),
      );
    } else if (requestMethod == RequestMethod.POST) {
      response = await dio.post(
        path,
        queryParameters: param,
        data: body,
        onReceiveProgress: onProgress,
        options: Options(headers: sendHeader ? await getBaseHeader() : null),
      );
    } else if (requestMethod == RequestMethod.PUT) {
      response = await dio.put(
        path,
        queryParameters: param,
        data: body,
        onReceiveProgress: onProgress,
        options: Options(headers: sendHeader ? await getBaseHeader() : null),
      );
    } else {
      response = await dio.delete(
        path,
        queryParameters: param,
        data: body,
        options: Options(headers: sendHeader ? await getBaseHeader() : null),
      );
    }
    return response.data;
  }

  @override
  void showError(String error, int statusCode) {}
}
