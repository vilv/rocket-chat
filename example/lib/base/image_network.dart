import 'package:cached_network_image/cached_network_image.dart';
import 'package:example/main.dart';
import 'package:flutter/material.dart';

class ImageNetWork extends StatelessWidget {
  final String url;
  final double? height;
  final double? width;
  final BoxFit? fit;
  const ImageNetWork(
      {Key? key, required this.url, this.height, this.width, this.fit})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    print(url);
    return CachedNetworkImage(
      imageUrl: url,
      height: height,
      width: width,
      fit: fit,
      httpHeaders: {"X-Auth-Token": tokenRocket, "X-User-Id": userIdRocket},
      errorWidget: (context, url, error) {
        return Container(
          width: width,
          height: height,
          color: Colors.grey.withOpacity(0.5),
          child: Center(
            child: Icon(
              Icons.image,
              size: 26,
              color: Colors.grey[300],
            ),
          ),
        );
      },
    );
  }
}
