import 'package:base_getx/base_getx.dart';
import 'package:example/chat/chat_repository.dart';
import 'package:example/message/web_socket.dart';
import 'package:rocket_chat_connector_flutter/models/model_export.dart';

class DepartmentController extends BaseGetXController {
  RxList<Department> departments = RxList<Department>([]);
  final _repository = ChatRepository();
  int pageIndex = 0;
  @override
  void onInit() {
    getDepartment();
    super.onInit();
  }

  void getDepartment() async {
    try {
      var res = await _repository.getDepartments(pageIndex);
      departments.value = res;
    } catch (e) {
      print(e);
    }
  }
}
