import 'package:base_getx/base_getx.dart';
import 'package:base_getx/src/base/base_get.dart';
import 'package:example/base/base_getview.dart';
import 'package:example/department/department_ctr.dart';
import 'package:flutter/material.dart';

class DepartmentPage extends BaseGetWidget<DepartmentController> {
  @override
  DepartmentController initController() {
    return Get.put(DepartmentController());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Departments'),
      ),
      body: Obx(() => ListView.separated(
          itemCount: controller.departments.length,
          separatorBuilder: (context, index) {
            return Divider();
          },
          itemBuilder: (context, index) {
            return ListTile(
              title: Text(controller.departments.elementAt(index).name),
            );
          })),
    );
  }
}
