import 'package:base_getx/base_getx.dart';
import 'package:example/chat/chat_repository.dart';
import 'package:example/main.dart';
import 'package:rocket_chat_connector_flutter/models/model_export.dart';

class ChatController extends BaseGetXController {
  final _rocketRepo = ChatRepository();
  Rx<List<ChannelRocket>> channels = Rx([]);
  RxList<Room> rooms = RxList([]);
  // final ChatSocket chatSocket = ChatSocket();
  void loginRocketChat(String userName, String password) async {
    var res = await _rocketRepo.loginRocketChat(userName, password);
    if (res.loginData != null) {
      tokenRocket = res.loginData!.authToken;
      userIdRocket = res.loginData!.userId;
    }
  }

  @override
  void onReady() {
    // chatSocket.initSocketChannel();
    // chatSocket.streamNotifyAll();
    // chatSocket.webSocketChannel.stream.listen((event) {
    //   print('event $event');
    // });
    // getChannels();
    getRooms();
    super.onReady();
  }

  @override
  void onClose() {
    // chatSocket.closeWebSocket();
    super.onClose();
  }

  void getChannels() async {
    try {
      var res = await _rocketRepo.getChannels();
      channels.value = res;
    } catch (e) {
      print(e);
    }
  }

  void getRooms() async {
    try {
      var res = await _rocketRepo.getRooms();
      rooms.value = res;
    } catch (e) {
      print(e);
    }
  }

  void setRoomNewMessage(Room newRoom) {
    print('Replace room to first index of list...');
    int indexReplace = -1;
    for (int index = 0; index < rooms.length; index++) {
      if (rooms[index].id == newRoom.id) {
        indexReplace = index;
        break;
      }
    }
    if (indexReplace != -1) {
      rooms.removeAt(indexReplace);
      rooms.insert(0, newRoom);
    }
    rooms.refresh();
  }
}
