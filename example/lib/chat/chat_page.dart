import 'dart:convert';

import 'package:base_getx/base_getx.dart';
import 'package:example/base/base_getview.dart';
import 'package:example/chat/chat_controller.dart';
import 'package:example/main.dart';
import 'package:example/message/message_page.dart';
import 'package:example/search/searcg_base.dart';
import 'package:example/search/search_user.dart';
import 'package:example/utils/convert.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

final String serverUrl = "http://172.21.208.1:3000/";
final String webSocketUrl = "ws://172.21.208.1:3000/websocket";
final String username = "demo1";
final String password = "123456";

class ChatPage extends BaseGetWidget<ChatController> {
  @override
  ChatController initController() {
    return Get.put(ChatController());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("All Conversation"),
        actions: [
          IconButton(
              onPressed: () {
                showSearchCustom(
                    context: context, delegate: CustomSearchDelegate());
              },
              icon: Icon(Icons.search))
        ],
      ),
      body: Obx(() => ListView.builder(
            itemCount: controller.rooms.length,
            itemBuilder: (context, index) {
              return ListTile(
                onTap: () {
                  controller.rooms[index].countUnread = 0;
                  controller.rooms.refresh();
                  Get.to(() => MessagePage(), arguments: {
                    "roomId": controller.rooms[index].id,
                    "roomName": controller.rooms[index].name,
                    "isChannel": controller.rooms[index].isChannel
                  });
                },
                title: Text('${controller.rooms[index].name}'),
                subtitle: userName != controller.rooms[index].userNameSend
                    ? Text(
                        'Gửi từ ${controller.rooms[index].userSend} lúc ${convertDateToString2(controller.rooms[index].timeSend)}')
                    : Text(
                        '${convertDateToString2(controller.rooms[index].timeSend)}'),
                trailing: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text('${controller.rooms[index].lastMessage}'),
                    SizedBox(
                      height: 5,
                    ),
                    Visibility(
                      visible: controller.rooms[index].countUnread > 0,
                      child: Container(
                        // height: 10,
                        // width: 10,
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.red),
                        child: Text(
                          'new',
                          style: TextStyle(color: Colors.white, fontSize: 10),
                        ),
                      ),
                    )
                  ],
                ),
              );
            },
          )),
    );
  }
}
