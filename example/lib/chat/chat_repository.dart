import 'dart:convert';

import 'package:base_getx/base_getx.dart' as base;
import 'package:dio/dio.dart';
import 'package:example/base/consts.dart';
import 'package:example/base/services.dart';
import 'package:mime/mime.dart';
import 'package:rocket_chat_connector_flutter/services/rocket_repository.dart';
import 'package:rocket_chat_connector_flutter/models/model_export.dart';

// import 'package:rocket_chat_connector_flutter/models/room.dart';
import 'package:http_parser/http_parser.dart';

class ChatRepository extends RocketRepository {
  final _rocketService = RocketService();

  @override
  Future<LoginResponse> loginRocketChat(
      String username, String password) async {
    var res = await _rocketService.sendRequest(
        path: RocketConst.loginUrl,
        requestMethod: base.RequestMethod.POST,
        sendHeader: false,
        body: {"user": username, "password": password});
    if (res == null) return LoginResponse(message: 'Lỗi đăng nhập');
    return LoginResponse.fromMap(res);
  }

  @override
  Future<List<ChannelRocket>> getChannels() async {
    var res = await _rocketService.sendRequest(
      path: RocketConst.channelsUrl,
      requestMethod: base.RequestMethod.GET,
    );
    return List<ChannelRocket>.from(
        res["channels"].map((x) => ChannelRocket.fromJson(x)));
  }

  @override
  Future<List<Room>> getRooms() async {
    var res = await _rocketService.sendRequest(
      path: RocketConst.getRoomsUrl,
      requestMethod: base.RequestMethod.GET,
    );
    return List<Room>.from(res["update"].map((x) => Room.fromMap(x)));
  }

  @override
  Future<List<MessageChat>> getMessages(
      String roomId, int pageIndex, bool isChannel) async {
    var res = await _rocketService.sendRequest(
        path: isChannel
            ? RocketConst.messageByChannelUrl
            : RocketConst.messageByUserUrl,
        requestMethod: base.RequestMethod.GET,
        param: {
          "roomId": roomId,
          "offset": pageIndex * 10,
          "count": 10,
        });
    if (res['messages'] == null || res['count'] == 0) return [];
    return List<MessageChat>.from(
        res["messages"].map((x) => MessageChat.fromJson(x)));
  }

  @override
  Future<bool> uploadFile(String roomId, String path, String describe) async {
    final mimeType = lookupMimeType(path);
    final multiPathFile = await MultipartFile.fromFile(path,
        filename: path.split('/').last,
        contentType:
            MediaType(mimeType!.split('/').first, mimeType.split('/').last));
    var formData = FormData.fromMap({
      "description": describe,
      "file": multiPathFile,
    });
    var res = await _rocketService.sendRequest(
        path: RocketConst.uploadRoomsUrl + '/$roomId',
        requestMethod: base.RequestMethod.POST,
        body: formData);
    return res['success'] ?? false;
  }

  @override
  Future<bool> saveTokenToServer(String fcmToken) async {
    var res = await _rocketService.sendRequest(
        path: RocketConst.saveTokenNotiUrl,
        requestMethod: base.RequestMethod.POST,
        body: {
          "type": "gcm",
          "value": fcmToken,
          "appName": 'com.example.rocketchat',
        });
    return res['success'] ?? false;
  }

  Future<List<User>> findUserByText(String textSearch) async {
    var query = {
      "query": jsonEncode({
        "name": {"\$regex": "$textSearch"}
      })
    };
    return await getUsers(query);
    // try {
    //   var res = await _rocketService.sendRequest(
    //       path: RocketConst.getUserUrl,
    //       requestMethod: base.RequestMethod.GET,
    //       param: query);
    //   return List<User>.from(res["users"].map((x) => User.fromJson(x)));
    // } catch (e) {
    //   print(e);
    //   return [];
    // }
  }

  @override
  Future<List<Department>> getDepartments(int pageIndex) async {
    var res = await _rocketService.sendRequest(
        path: RocketConst.liveChatDepartmentUrl,
        requestMethod: base.RequestMethod.GET,
        param: {
          "offset": pageIndex * 10,
          "count": 10,
        });
    if (res['departments'] == null || res['success'] == false) return [];
    return List<Department>.from(
        res["departments"].map((x) => Department.fromMap(x)));
  }

  @override
  Future<List<User>> getUsers(Map<String, dynamic> query) async{
    try {
      var res = await _rocketService.sendRequest(
          path: RocketConst.getUserUrl,
          requestMethod: base.RequestMethod.GET,
          param: query);
      return List<User>.from(res["users"].map((x) => User.fromJson(x)));
    } catch (e) {
      print(e);
      return [];
    }
  }

  @override
  Future<bool> registerAccount(RegisterReq registerReq) async {
    var res = await _rocketService.sendRequest(
        path: RocketConst.registerUrl,
        requestMethod: base.RequestMethod.POST,
        sendHeader: false,
        body: registerReq.toMap());
    return res['success'] ?? false;
  }
}
