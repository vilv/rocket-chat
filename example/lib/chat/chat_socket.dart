// import 'dart:convert';
//
// import 'package:example/base/services.dart';
// import 'package:example/main.dart';
// import 'package:web_socket_channel/io.dart';
// import 'package:web_socket_channel/web_socket_channel.dart';
//
// class ChatSocket {
//   late WebSocketChannel webSocketChannel;
//   void initSocketChannel() {
//     webSocketChannel = IOWebSocketChannel.connect(RocketService.webSocketUrl);
//     webSocketChannel.sink.add(jsonEncode({
//       "msg": "connect",
//       "version": "1",
//       "support": ["1", "pre2", "pre1"]
//     }));
//     webSocketChannel.sink.add(jsonEncode({
//       "msg": "method",
//       "method": "login",
//       "id": "42",
//       "params": [
//         {"resume": tokenRocket}
//       ]
//     }));
//   }
//
//   void getRooms() {
//     final json = {
//       "msg": "method",
//       "method": "rooms/get",
//       "id": "42",
//       "params": [
//         {"\$date": DateTime.now().millisecondsSinceEpoch}
//       ]
//     };
//     webSocketChannel.sink.add(jsonEncode(json));
//   }
//
//   void streamNotifyAll() {
//     final map = {
//       "msg": "sub",
//       "id": userIdRocket,
//       "name": "stream-notify-all",
//       "params": ["event", false]
//     };
//     webSocketChannel.sink.add(jsonEncode(map));
//   }
//
//   void closeWebSocket() {
//     webSocketChannel.sink.close();
//   }
// }
