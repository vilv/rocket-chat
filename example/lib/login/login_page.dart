import 'dart:io';

import 'package:base_getx/base_getx.dart';
import 'package:example/base/base_getview.dart';
import 'package:example/base/input_text.dart';
import 'package:example/department/department_page.dart';
import 'package:example/live_chat/live_chat_page.dart';
import 'package:example/login/login_controller.dart';
import 'package:example/register/register_page.dart';
import 'package:example/utils/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class LoginPage extends BaseGetWidget<LoginController> {
  @override
  Widget build(BuildContext context) {
    return buildLoading(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Login to RocketChat'),
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Form(
            key: controller.formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InputText(
                  hintText: 'Nhập username',
                  controller: controller.userNameController,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Username không hợp lệ';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                InputText(
                  hintText: 'Nhập mật khẩu',
                  obscureText: true,
                  controller: controller.passController,
                  textInputAction: TextInputAction.done,
                  inputFormatters: [
                    FilteringTextInputFormatter.deny(RegExp(r' ')),
                    LengthLimitingTextInputFormatter(16),
                  ],
                  validator: (value) {
                    if (value!.length < 6) {
                      return 'Mật khẩu ít nhất 6 kí tự';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                    onPressed: () {
                      if (controller.formKey.currentState!.validate()) {
                        controller.loginRocketChat();
                      }
                    },
                    style: ElevatedButton.styleFrom(fixedSize: Size(200, 40)),
                    child: Text(
                      'Login',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    )),
                // Padding(
                //   padding: const EdgeInsets.symmetric(vertical: 20),
                //   child: _loginWithAnounymous(),
                // ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                    onPressed: () {
                      Get.to(() => RegisterPage());
                    },
                    style: ElevatedButton.styleFrom(
                        fixedSize: Size(200, 40),
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5),
                            side: BorderSide(color: Colors.blue))),
                    child: Text(
                      'Register',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ))
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _loginWithAnounymous() {
    return ElevatedButton(
        onPressed: () {
          Get.to(() => DepartmentPage());
        },
        child: Text('Guest'));
  }

  @override
  LoginController initController() {
    return Get.put(LoginController());
  }
}
