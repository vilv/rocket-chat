import 'package:base_getx/base_getx.dart';
import 'package:example/base/notification.dart';
import 'package:example/chat/chat_page.dart';
import 'package:example/chat/chat_repository.dart';
import 'package:example/main.dart';
import 'package:flutter/material.dart';

class LoginController extends BaseGetXController {
  final formKey = GlobalKey<FormState>();
  final userNameController = TextEditingController(text: 'demo1');
  final passController = TextEditingController(text: '123456');
  final _rocketRepo = ChatRepository();
  void loginRocketChat() async {
    setLoading(true);
    var res = await _rocketRepo.loginRocketChat(
        userNameController.text, passController.text);
    if (res.loginData != null) {
      tokenRocket = res.loginData!.authToken;
      userIdRocket = res.loginData!.userId;
      userName = res.loginData!.userName;
      NotificationApp.initNotification(() {});
      Get.to(() => ChatPage());
    }
    setLoading(false);
  }
}
