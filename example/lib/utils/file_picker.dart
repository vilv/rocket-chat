import 'package:file_picker/file_picker.dart';
import 'package:image_picker/image_picker.dart';

class PickerFile {
  final ImagePicker _picker = ImagePicker();
  Future<String?> pickImageFromGalery() async {
    final result = await _picker.pickImage(source: ImageSource.gallery);
    if (result != null) {
      return result.path;
    }
    return null;
  }

  Future<String?> pickImageFromCamera() async {
    final result = await _picker.pickImage(source: ImageSource.camera);
    if (result != null) {
      return result.path;
    }
    return null;
  }

  Future<String?> pickFileFromStorage() async {
    final result = await FilePicker.platform.pickFiles();
    if (result != null) {
      return result.paths.first;
    }
    return null;
  }
}
