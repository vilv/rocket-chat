import 'package:intl/intl.dart';

String convertDateToString1(DateTime date) {
  return DateFormat('dd/MM/yy HH:mm').format(date);
}
String convertDateToString2(DateTime date) {
  return DateFormat('HH:mm dd/MM/yy').format(date);
}

String convertDateToString3(DateTime date) {
  return DateFormat('HH:mm a').format(date);
}
