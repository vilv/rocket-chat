import 'dart:async';

import 'package:base_getx/base_getx.dart';
import 'package:example/chat/chat_repository.dart';
import 'package:example/message/message_page.dart';
import 'package:rocket_chat_connector_flutter/models/model_export.dart';
import 'package:example/search/searcg_base.dart';
import 'package:flutter/material.dart';

class CustomSearchDelegate extends CustomSearch {
  List<User> users = [];
  StreamController<List<User>> userController = StreamController<List<User>>();
  Stream<List<User>> get userStream => userController.stream;
  final _repo = ChatRepository();

  bool isFirst = true;
  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
          onPressed: () {
            query = '';
          },
          icon: Icon(Icons.close))
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return BackButton(
      onPressed: () => Get.back(),
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return ListView.builder(
      itemCount: users.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(users[index].name),
          trailing: Text(users[index].status),
        );
      },
    );
  }

  @override
  void close(BuildContext context, result) {
    userController.close();
    super.close(context, result);
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return StreamBuilder<List<User>>(
      stream: userStream,
      builder: (context, snapshot) {
        List<User> datas = [];
        if (snapshot.hasData) {
          datas = snapshot.data ?? [];
        }
        return ListView.builder(
            itemCount: datas.length,
            itemBuilder: (context, index) {
              return ListTile(
                onTap: () {
                  Get.back();
                  Get.to(() => MessagePage(), arguments: {
                    'roomName': datas[index].name,
                    'username': datas[index].username,
                  });
                },
                title: Text(datas[index].name),
                trailing: Text(datas[index].status),
              );
            });
      },
    );
  }

  @override
  void onChange(String value) async {
    if (value.isNotEmpty) {
      await Future.delayed(Duration(milliseconds: 200));
      var res = await _repo.findUserByText(query);
      userController.sink.add(res);
    }
  }

  @override
  void onSubmit(String value) async {
    if (value.isNotEmpty) {
      var res = await _repo.findUserByText(query);
      users = res;
    }
  }
}
