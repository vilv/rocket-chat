// import 'package:firebase_messaging/firebase_messaging.dart';
// import 'package:flutter_local_notifications/flutter_local_notifications.dart';

// class NotificationApp {
//   static final FirebaseMessaging messaging = FirebaseMessaging.instance;
//   static final localNotification = FlutterLocalNotificationsPlugin();
//   static Future<void> initNotification(void Function() onClickNoti) async {
//     var response = await messaging.requestPermission(
//       alert: true,
//       announcement: true,
//       badge: true,
//       carPlay: false,
//       criticalAlert: false,
//       provisional: false,
//       sound: true,
//     );
//     messaging.setAutoInitEnabled(true);
//     messaging.setForegroundNotificationPresentationOptions(
//         alert: true, badge: true, sound: true);
//     const AndroidInitializationSettings initializationSettingsAndroid =
//         AndroidInitializationSettings('@drawable/ic_launcher');
//     final IOSInitializationSettings initializationSettingsIOS =
//         IOSInitializationSettings();
//     final InitializationSettings initializationSettings =
//         InitializationSettings(
//             android: initializationSettingsAndroid,
//             iOS: initializationSettingsIOS);
//     localNotification.initialize(
//       initializationSettings,
//       onSelectNotification: (payload) async {
//         print('onselected');
//         onClickNoti();
//         return '';
//       },
//     );

//     FirebaseMessaging.onMessage.listen((event) {
//       print("nhan thong bao...");
//       // if (event.notification != null && !isUserInChat) {
//       //   showNotification(
//       //       event.notification!.title ?? '', event.notification!.body ?? '');
//       // }
//     });
//     FirebaseMessaging.onMessageOpenedApp.listen((event) {});
//   }

//   static void showNotification(String title, String body) async {
//     if (title.isNotEmpty && body.isNotEmpty) {
//       var androidPlatformChannelSpecifics = AndroidNotificationDetails(
//           'default_notification_channel_id', 'default_notification_channel',
//           importance: Importance.max,
//           priority: Priority.high,
//           icon: 'ic_launcher',
//           ticker: 'ticker',
//           playSound: true);
//       var iOSPlatformChannelSpecifics = IOSNotificationDetails();
//       var platformChannelSpecifics = NotificationDetails(
//         android: androidPlatformChannelSpecifics,
//         iOS: iOSPlatformChannelSpecifics,
//       );
//       localNotification.show(0, title, body, platformChannelSpecifics);
//     }
//   }
// }
