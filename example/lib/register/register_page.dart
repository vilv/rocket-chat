import 'package:base_getx/base_getx.dart';
import 'package:example/base/base_getview.dart';
import 'package:example/base/input_text.dart';
import 'package:example/register/register_controller.dart';
import 'package:flutter/material.dart';

class RegisterPage extends BaseGetWidget<RegisterController> {
  @override
  RegisterController initController() {
    return Get.put(RegisterController());
  }

  @override
  Widget build(BuildContext context) {
    return buildLoading(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Register an account RocketChat'),
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
          child: Form(
            key: controller.formKey,
            child: Column(
              children: [
                InputText(
                  hintText: 'Tên',
                  controller: controller.nameController,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Tên không được để trống';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                InputText(
                  hintText: 'Username',
                  controller: controller.userNameController,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Username không được để trống';
                    }
                    return null;
                  },
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: InputText(
                    hintText: 'E-mail',
                    controller: controller.emailController,
                    validator: (value) {
                      if (isEmailInvalid(value!)) {
                        return 'Email không hợp lệ';
                      }
                      return null;
                    },
                  ),
                ),
                InputText(
                  hintText: 'Mật khẩu',
                  controller: controller.passwordController,
                  obscureText: true,
                  validator: (value) {
                    if (value!.length > 16 || value.length < 6) {
                      return 'Mật khẩu không hợp lệ';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                InputText(
                  hintText: 'Nhập lại mật khẩu',
                  obscureText: true,
                  validator: (value) {
                    if (!isSamePassword(controller.passwordController.text,
                        controller.rePasswordController.text)) {
                      return 'Nhập lại mật khẩu không đúng';
                    }
                    return null;
                  },
                  controller: controller.rePasswordController,
                ),
                SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                    onPressed: () {
                      if (controller.formKey.currentState!.validate()) {
                        controller.registerAccount();
                      }
                    },
                    style: ElevatedButton.styleFrom(
                        fixedSize: Size(200, 40),
                        primary: Colors.blue,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5),
                            side: BorderSide(color: Colors.blue))),
                    child: Text('Đăng kí',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.bold)))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
