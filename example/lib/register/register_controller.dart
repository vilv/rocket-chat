import 'package:base_getx/base_getx.dart';
import 'package:example/chat/chat_repository.dart';
import 'package:rocket_chat_connector_flutter/models/model_export.dart';
import 'package:flutter/cupertino.dart';

class RegisterController extends BaseGetXController {
  final nameController = TextEditingController();
  final userNameController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final rePasswordController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  final _repository = ChatRepository();
  void registerAccount() async {
    setLoading(true);
    try {
      var res = await _repository.registerAccount(RegisterReq(
          userName: userNameController.text,
          email: emailController.text,
          name: nameController.text,
          password: passwordController.text));
      setLoading(false);
      if (res) {
        ShowDialog().showDialogNotification(
            title: 'Thông báo',
            content: 'Đăng kí tài khoản thành công',
            onClick: () {
              Get.back();
            });
      } else {
        ShowDialog().showErrorDialog(
          title: 'Thông báo',
          content: 'Có lỗi xảy ra',
        );
      }
    } catch (e) {
      setLoading(false);
    }
  }
}
