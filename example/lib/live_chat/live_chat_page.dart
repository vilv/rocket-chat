import 'package:base_getx/base_getx.dart';
import 'package:example/base/base_getview.dart';
import 'package:example/live_chat/live_chat_ctr.dart';
import 'package:flutter/material.dart';

class LiveChatPage extends BaseGetWidget<LChatController> {
  @override
  LChatController initController() {
    return Get.put(LChatController());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Live chat Rocket'),
      ),
      // body: StreamBuilder(
      //   stream: controller.webSocket.webSocketChannel.stream,
      //   builder: (context, snap) {
      //     if (snap.hasData) {
      //       print(snap.data);
      //     }
      //     return Center(child: Text('Data'));
      //   },
      // ),
    );
  }
}
