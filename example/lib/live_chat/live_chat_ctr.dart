import 'package:base_getx/base_getx.dart';
import 'package:example/message/web_socket.dart';

class LChatController extends BaseGetXController {
  void getInitialData() {}

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    getInitialData();
    super.onReady();
  }
}
