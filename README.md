# rocket-chat-connector-flutter

Flutter Rocket.chat connector

## Getting Started
### 1. Giới thiệu về RocketChat
[Rocket.Chat](https://rocket.chat/) là một open-source communications platform có thể customize hoàn toàn và được phát triển bằng JavaScript dành cho các tổ chức, doanh nghiệp có tiêu chuẩn cao về bảo mật dữ liệu.
Nó được tích hợp với nhiều kênh trò chuyện như trò chuyện trực tiếp trên trang web, email, trang Facebook, Twitter, WhatsApp, Instagram, v.v.
Xem hướng dẫn sử dụng RocketChat chi tiết tại [document](https://docs.rocket.chat/)
Hướng dẫn dành cho developer xem tại [đây](https://developer.rocket.chat/)
### 2. Rocket WebSocket
 **Rocket Chat** sử dụng WebSocket để connect đến server realtime
 * Kết nối đến server Rocket
 ```
 void initSocketChannel({required String tokenRocket}) {
     _tokenUser = tokenRocket;
     webSocketChannel = IOWebSocketChannel.connect(webSocketUrl);
     webSocketChannel.sink.add(jsonEncode({
       "msg": "connect",
       "version": "1",
       "support": ["1", "pre2", "pre1"]
     }));
     webSocketChannel.sink.add(jsonEncode({
       "msg": "method",
       "method": "login",
       "id": "42",
       "params": [
         {"resume": tokenRocket}
       ]
     }));
   }
 ```
 * Lấy danh sách rooms chat của user
  ```
  void getRooms() {
      final json = {
        "msg": "method",
        "method": "rooms/get",
        "id": "42",
        "params": [
          {"\$date": DateTime.now().millisecondsSinceEpoch}
        ]
      };
      webSocketChannel.sink.add(jsonEncode(json));
    }
  ```
  * Tạo một cuộc hội thoại đơn( 1vs1 )
  ```
  void createDirectMessage(String userName) {
      webSocketChannel.sink.add(jsonEncode({
        "msg": "method",
        "id": "42",
        "method": "createDirectMessage",
        "params": [userName]
      }));
    }
  ```
  * Gửi tin nhắn đến [channelId]
  ```
   void sendMessageOnChannel(String message, String channelId) {
      print("send msg = $message at channel $channelId");
      webSocketChannel.sink.add(jsonEncode({
        "msg": "method",
        "method": "sendMessage",
        "id": "42",
        "params": [
          {"rid": channelId, "msg": message}
        ]
      }));
    }
  ```
  * Subscribe một stream của User thông qua [userId], dùng để listen các thông báo của User
  ```
   void streamNotifyUserSubscribe(String userId) {
       webSocketChannel.sink.add(jsonEncode({
         "msg": "sub",
         "id": userId + "subscription-id",
         "name": "stream-notify-user",
         "params": [userId + "/notification", false]
       }));
     }
  ```
  * Subscribe một stream của channel thông qua [channelId], dùng để listen các message của channel này
  ```
   void streamChannelMessagesSubscribe(String channelId) {
      webSocketChannel.sink.add(jsonEncode({
        "msg": "sub",
        "id": channelId,
        "name": "stream-room-messages",
        "params": [channelId, false]
      }));
    }
  ```
  ### 3. Rocket Repository
  Một abstract class chứa các function để connect đến RESTful API của **Rocket Chat**
  Khi extends class này thì cần implement các function sau:
  * Đăng nhập vào server Rocket
  ```
  Future<LoginResponse> loginRocketChat(String username, String password);
  ```
  * Get tất cả channels có trên server
  ```
  Future<List<ChannelRocket>> getChannels();
  ```
  * Get tất cả cuộc hội thoại của user
  ```
  Future<List<Room>> getRooms();
  ```
  * Get danh sách tin nhắn
  ```
   Future<List<MessageChat>> getMessages(
        String roomId, int pageIndex, bool isChannel);
  ```
  * Upload ảnh, file lên server
  ```
   Future<bool> uploadFile(String roomId, String path, String describe);
  ```
   * Đăng kí tài khoản mới
  ```
   Future<bool> registerAccount(RegisterReq registerReq);
  ```
   * Lưu token FCM lên server
  ```
   Future<bool> saveTokenToServer(String fcmToken);
  ```
   * Lưu token FCM lên server
  ```
   Future<List<User>> getUsers(Map<String, dynamic> query);
  ```
   ### 4. Ví dụ về việc implement Rocket Repository (xem trong project example)
   ```
   class ChatRepository extends RocketRepository {
   ...
   }
   ```