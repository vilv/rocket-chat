import 'dart:convert';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class RocketWebSocket {
  static final _instance = RocketWebSocket._internal();

  RocketWebSocket._internal();

  static String webSocketUrl = '';
  static String serverUrl = '';

  factory RocketWebSocket({required String mServerUrl,required String mWebSocketUrl}) {
    webSocketUrl = mWebSocketUrl;
    serverUrl = mServerUrl;
    return _instance;
  }

  String? _tokenUser;
  late WebSocketChannel webSocketChannel;

  /// Khởi tạo, đăng nhập đến [webSocketChannel] của [Rocket]
  ///
  /// [tokenRocket] có được sau khi đăng nhập vào server của [Rocket]
  void initSocketChannel({required String tokenRocket}) {
    _tokenUser = tokenRocket;
    webSocketChannel = IOWebSocketChannel.connect(webSocketUrl);
    webSocketChannel.sink.add(jsonEncode({
      "msg": "connect",
      "version": "1",
      "support": ["1", "pre2", "pre1"]
    }));
    webSocketChannel.sink.add(jsonEncode({
      "msg": "method",
      "method": "login",
      "id": "42",
      "params": [
        {"resume": tokenRocket}
      ]
    }));
  }

  /// Subscribe một stream của channel thông qua [channelId], dùng để listen các message của channel này
  ///
  /// Link: {@youtube 560 315 https://developer.rocket.chat/reference/api/realtime-api/livechat-api/streamlivechatroom#ddp-message}
  void streamChannelMessagesSubscribe(String channelId) {
    webSocketChannel.sink.add(jsonEncode({
      "msg": "sub",
      "id": channelId,
      "name": "stream-room-messages",
      "params": [channelId, false]
    }));
  }

  /// Subscribe một stream của User thông qua [userId], dùng để listen các thông báo của User
  ///
  /// Link: {@youtube 560 315 https://developer.rocket.chat/reference/api/realtime-api/subscriptions/stream-notify-user}
  void streamNotifyUserSubscribe(String userId) {
    webSocketChannel.sink.add(jsonEncode({
      "msg": "sub",
      "id": userId + "subscription-id",
      "name": "stream-notify-user",
      "params": [userId + "/notification", false]
    }));
  }

  // void streamLiveChatRoom(String id) {
  //   webSocketChannel.sink.add(jsonEncode({
  //     "msg": "sub",
  //     "id":id,
  //     "name": "stream-room-messages",
  //     "params": [userId + "/notification", false]
  //   }));
  // }
  /// Gửi tin nhắn đến [channelId]
  ///
  /// Link: {@youtube 560 315 https://developer.rocket.chat/reference/api/realtime-api/method-calls/send-message}
  void sendMessageOnChannel(String message, String channelId) {
    print("send msg = $message at channel $channelId");
    webSocketChannel.sink.add(jsonEncode({
      "msg": "method",
      "method": "sendMessage",
      "id": "42",
      "params": [
        {"rid": channelId, "msg": message}
      ]
    }));
  }

  // TODO: đang hoàn thiện LiveChat
  void getInitialDataLiveChat() {
    webSocketChannel.sink.add(jsonEncode({
      "msg": "method",
      "method": "livechat:getInitialData",
      "params": [_tokenUser],
      "id": "1"
    }));
  }

// TODO: đang hoàn thiện LiveChat
  void sendMessageToLiveChat(String content, String id) {
    print("send msg = $content by guest accout with id $id");
    webSocketChannel.sink.add(jsonEncode({
      "msg": "method",
      "method": "sendMessageLivechat",
      "id": "11",
      "params": [
        {"_id": id, "rid": id, "msg": content, "token": _tokenUser}
      ]
    }));
  }

// TODO: đang hoàn thiện LiveChat
  void registerGuestAccount(String departmentId) {
    webSocketChannel.sink.add(jsonEncode({
      "msg": "method",
      "method": "livechat:registerGuest",
      "params": [
        {
          "token": _tokenUser,
          "name": "Guest Name",
          "email": "guest@rocket.chat",
          "department": departmentId
        }
      ],
      "id": "5"
    }));
  }

  /// Get danh sách room chat mà user đang tham gia
  ///
  /// Link: {@youtube 560 315 https://developer.rocket.chat/reference/api/realtime-api/method-calls/get-rooms#example-call}
  void getRooms() {
    final json = {
      "msg": "method",
      "method": "rooms/get",
      "id": "42",
      "params": [
        {"\$date": DateTime.now().millisecondsSinceEpoch}
      ]
    };
    webSocketChannel.sink.add(jsonEncode(json));
  }

  /// Tạo một cuộc hội thoại đơn( 1vs1 )
  ///
  /// [userName]: username của tài khoản muốn chat
  ///
  /// Link: {@youtube 560 315 https://developer.rocket.chat/reference/api/realtime-api/method-calls/create-direct-message}
  void createDirectMessage(String userName) {
    webSocketChannel.sink.add(jsonEncode({
      "msg": "method",
      "id": "42",
      "method": "createDirectMessage",
      "params": [userName]
    }));
  }

  void closeWebSocket() {
    webSocketChannel.sink.close();
  }
}
