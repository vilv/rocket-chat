import 'package:rocket_chat_connector_flutter/models/model_export.dart';

abstract class RocketRepository {
  /// Đăng nhập vào server Rocket
  ///
  /// EndPoint [/api/v1/login], link tham khảo: {@youtube 560 315 https://developer.rocket.chat/reference/api/rest-api/endpoints/other-important-endpoints/authentication-endpoints/login}
  Future<LoginResponse> loginRocketChat(String username, String password);

  /// Get tất cả channels có trên server
  ///
  /// Link: {@youtube 560 315 https://developer.rocket.chat/reference/api/rest-api/endpoints/team-collaboration-endpoints/channels-endpoints/list}
  Future<List<ChannelRocket>> getChannels();

  /// Get tất cả cuộc hội thoại của user
  ///
  ///  Link: {@youtube 560 315 https://developer.rocket.chat/reference/api/rest-api/endpoints/team-collaboration-endpoints/rooms-endpoints/change-archivation-state}
  Future<List<Room>> getRooms();

  /// Get danh sách tin nhắn
  ///
  /// [isChannel] = true: get danh sách tin nhắn của kênh chat
  ///
  /// Link: {@youtube 560 315 https://developer.rocket.chat/reference/api/rest-api/endpoints/team-collaboration-endpoints/channels-endpoints/messages#example-call}
  ///
  /// [isChannel] = false: get danh sách tin nhắn của user cụ thể(direct message) - tin nhắn 1vs1
  ///
  /// Link: {@youtube 560 315 https://developer.rocket.chat/reference/api/rest-api/endpoints/team-collaboration-endpoints/im-endpoints/messages#example-call}
  ///
  /// [pageIndex] là số page cần get về, bắt đầu từ 0
  ///
  /// [roomId] là id của Room sau khi get về từ [getRooms()]
  Future<List<MessageChat>> getMessages(
      String roomId, int pageIndex, bool isChannel);

  /// Upload ảnh, file lên server
  ///
  /// [roomId] là id của Room sau khi get về từ [getRooms()]
  ///
  /// [path]: đường dẫn tuyệt đối của file. Dùng [FormData] để gửi request lên server, convert qua [MultipartFile] trước khi add vào [FormData]
  ///
  /// [describe]: phần tin nhắn mô tả cho file, tương tự cách hoạt động của [Skype] khi chat
  ///
  /// Link: {@youtube 560 315 https://developer.rocket.chat/reference/api/rest-api/endpoints/team-collaboration-endpoints/rooms-endpoints/upload-file-to-a-room}
  Future<bool> uploadFile(String roomId, String path, String describe);

  /// Đăng kí tài khoản mới
  ///
  /// [registerReq]: body để gửi lên server gồm( [username], [email], [password], [name] )
  ///
  /// Link: {@youtube 560 315 https://developer.rocket.chat/reference/api/rest-api/endpoints/team-collaboration-endpoints/users-endpoints/register-users}
  Future<bool> registerAccount(RegisterReq registerReq);
  /// Lưu token FCM lên server
  ///
  /// Sử dụng để nhận push notification từ server Rocket
  ///
  /// Link: {@youtube 560 315 https://developer.rocket.chat/reference/api/rest-api/endpoints/team-collaboration-endpoints/push-token-endpoints/push-token}
  Future<bool> saveTokenToServer(String fcmToken);
  /// Get danh sách tất cả user có trên server
  ///
  /// [query]: param để lọc user
  ///
  /// Link: {@youtube 560 315 https://developer.rocket.chat/reference/api/rest-api/endpoints/team-collaboration-endpoints/users-endpoints/get-users-list}
  Future<List<User>> getUsers(Map<String, dynamic> query);
  // TODO: dùng cho liveChat
  Future<List<Department>> getDepartments(int pageIndex);
}
