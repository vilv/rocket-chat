class Department {
  String id;
  bool enable;
  String name;
  String description;
  Department({
    required this.id,
    required this.enable,
    required this.name,
    required this.description,
  });

  factory Department.fromMap(Map<String, dynamic> map) {
    return Department(
      id: map['_id'] ?? '',
      enable: map['enabled'] ?? false,
      name: map['name'] ?? '',
      description: map['description'] ?? '',
    );
  }
}
