import 'dart:convert';

// class Notification {
//   NotificationType? msg;
//   String? collection;
//   String? serverId;
//   List<String>? subs;
//   List<String>? methods;
//   String? id;
//   NotificationFields? fields;
//   NotificationResult? result;

//   Notification({
//     this.msg,
//     this.collection,
//     this.serverId,
//     this.subs,
//     this.methods,
//     this.id,
//     this.fields,
//     this.result,
//   });

//   Notification.fromMap(Map<String, dynamic> json) {
//     if (json != null) {
//       msg = notificationTypeFromString(json['msg']);
//       collection = json['collection'];
//       serverId = json['server_id'];
//       subs = json['subs'] != null ? List<String>.from(json['subs']) : null;
//       methods =
//           json['methods'] != null ? List<String>.from(json['methods']) : null;
//       id = json["id"];
//       fields = json['fields'] != null
//           ? NotificationFields.fromMap(json["fields"])
//           : null;
//       result = json['result'] != null
//           ? NotificationResult.fromMap(json['result'])
//           : null;
//     }
//   }

//   @override
//   String toString() {
//     return 'Notification{msg: ${describeEnum(msg!)}, collection: $collection, serverId: $serverId, subs: $subs, methods: $methods, id: $id, fields: $fields, result: $result}';
//   }
// }
class MessageNoti {
  String userNameSend;
  String nameSend;
  String id;
  String channelId;
  String msg;
  DateTime timeSend;
  MessageNoti({
    required this.userNameSend,
    required this.nameSend,
    required this.id,
    required this.channelId,
    required this.msg,
    required this.timeSend,
  });

  factory MessageNoti.fromMap(Map<String, dynamic> map) {
    Map data = map['fields'];
    List<Map> args = data['args'];
    return MessageNoti(
      userNameSend: args.first['u']['username'] ?? '',
      nameSend: args.first['u']['name'] ?? '',
      id: args.first['_id'] ?? '',
      channelId: args.first['rid'] ?? '',
      msg: args.first['msg'] ?? '',
      timeSend: DateTime.fromMillisecondsSinceEpoch(args.first['ts']["\$date"]),
    );
  }
}
