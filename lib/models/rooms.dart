class Room {
  String name;
  String id;
  String userSend;
  String userNameSend;
  bool isChannel;
  int countUnread;
  String lastMessage;
  DateTime timeSend;
  String typeMessage;
  Room({
    required this.name,
    required this.id,
    required this.userSend,
    required this.isChannel,
    required this.lastMessage,
    this.userNameSend = '',
    required this.timeSend,
    this.countUnread =0,
    required this.typeMessage,
  });
  factory Room.copyWith(Room newRoom) {
    return Room(
        name: newRoom.name,
        id: newRoom.id,
        userNameSend: newRoom.userNameSend,
        userSend: newRoom.userSend,
        isChannel: newRoom.isChannel,
        lastMessage: newRoom.lastMessage,
        countUnread: newRoom.countUnread,
        timeSend: newRoom.timeSend,
        typeMessage: newRoom.typeMessage);
  }

  factory Room.fromMap(Map<String, dynamic> map) {
    if (map['lastMessage'] == null) {
      List usernames = map['usernames'];
      return Room(
          name: usernames.last,
          id: map['_id'] ?? '',
          userSend: usernames.last,
          isChannel: false,
          lastMessage: '',
          timeSend: DateTime.parse(map['ts']).add(Duration(hours: 7)),
          typeMessage: '');
    }
    String nameRoom = '';
    if (map['name'] != null) {
      nameRoom = map['name'];
    } else {
      List<String> userNames =
          List<String>.from(map["usernames"].map((x) => x));
      nameRoom = userNames.last;
    }
    List? mds = map['lastMessage']['md'];

    return Room(
      name: nameRoom,
      id: map['_id'] ?? '',
      userSend: map['lastMessage']['u']['name'] ?? '',
      userNameSend: map['lastMessage']['u']['username'] ?? '',
      isChannel: map['name'] != null,
      lastMessage: map['lastMessage']['msg'] ?? '',
      timeSend:
          DateTime.parse(map['lastMessage']['ts']).add(Duration(hours: 7)),
      typeMessage: mds == null ? '' : mds.first['type'],
    );
  }
}
