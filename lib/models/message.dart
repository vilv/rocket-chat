

import 'package:rocket_chat_connector_flutter/services/rocket_websocket.dart';

enum TypeFile { image, video, audio, application, text, another }

class MessageChat {
  MessageChat(
      {required this.id,
      required this.rid,
      required this.msg,
      required this.timeSend,
      required this.user,
      required this.updatedAt,
      required this.contents,
      this.fileMessage});

  String id;
  String rid;
  String msg;
  DateTime timeSend;
  DateTime updatedAt;
  UserSend user;
  FileMessage? fileMessage;
  List<ContentMessage> contents;
  String get content => contents.first.value.first.value;
  factory MessageChat.fromSnapShot(Map<String, dynamic> map) {
    Map data = map['fields'];
    List<dynamic> args = data['args'];
    return MessageChat(
        id: args.first['_id'] ?? '',
        rid: args.first['rid'] ?? '',
        msg: args.first['msg'] ?? '',
        fileMessage:
            args.first['file'] == null ? null : FileMessage.fromMap(args.first),
        timeSend:
            DateTime.fromMillisecondsSinceEpoch(args.first['ts']["\$date"]),
        user: UserSend.fromJson(args.first['u']),
        updatedAt: DateTime.now(),
        contents: []);
  }
  factory MessageChat.fromJson(Map<String, dynamic> json) => MessageChat(
        id: json["_id"],
        rid: json["rid"],
        msg: json["msg"],
        timeSend: DateTime.parse(json["ts"]).add(Duration(hours: 7)),
        user: UserSend.fromJson(json["u"]),
        fileMessage: json['file'] == null ? null : FileMessage.fromMap(json),
        updatedAt: DateTime.parse(json["_updatedAt"]),
        contents: json["md"] == null
            ? []
            : List<ContentMessage>.from(
                json["md"].map((x) => ContentMessage.fromJson(x))),
      );
  // factory MessageChat.fromJsonUser(Map<String, dynamic> json) {
  //   return MessageChat(
  //     id: json["_id"],
  //     rid: json["rid"],
  //     msg: json["msg"],
  //     timeSend: DateTime.parse(json["ts"]),
  //     user: UserSend.fromJson(json["u"]),
  //     updatedAt: DateTime.parse(json["_updatedAt"]),
  //     contents: List<ContentMessage>.from(
  //         json["md"].map((x) => ContentMessage.fromJson(x))),
  //   );
  // }
}

class ContentMessage {
  ContentMessage({
    required this.type,
    required this.value,
  });

  String type;
  List<Value> value;

  factory ContentMessage.fromJson(Map<String, dynamic> json) => ContentMessage(
        type: json["type"],
        value: List<Value>.from(json["value"].map((x) => Value.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "value": List<dynamic>.from(value.map((x) => x.toJson())),
      };
}

class Value {
  Value({
    required this.type,
    required this.value,
  });

  String type;
  String value;

  factory Value.fromJson(Map<String, dynamic> json) => Value(
        type: json["type"],
        value: json["value"],
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "value": value,
      };
}

class UserSend {
  UserSend({
    required this.id,
    required this.username,
    required this.name,
  });

  String id;
  String username;
  String name;

  factory UserSend.fromJson(Map<String, dynamic> json) => UserSend(
        id: json["_id"],
        username: json["username"] ?? '',
        name: json["name"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "username": username,
        "name": name,
      };
}

class FileMessage {
  String name;
  String type;
  String description;
  String url;
  ImageMessage? image;
  TypeFile typeFile;
  FileMessage(
      {required this.name,
      required this.type,
      required this.description,
      required this.url,
      this.image,
      required this.typeFile});

  factory FileMessage.fromMap(Map<String, dynamic> map) {
    List attachments = map['attachments'];
    String urlFile =
        RocketWebSocket.serverUrl + attachments.first['title_link'];
    ImageMessage? image;
    String mType = map['file']['type'] ?? '';
    TypeFile typeFile = TypeFile.text;
    if (mType.contains('image')) {
      typeFile = TypeFile.image;
      image = ImageMessage.fromMap(attachments.first);
    } else if (mType.contains('application')) {
      typeFile = TypeFile.application;
    } else if (mType.contains('video')) {
      typeFile = TypeFile.video;
    } else if (mType.contains('audio')) {
      typeFile = TypeFile.audio;
    }
    return FileMessage(
        name: map['file']['name'] ?? '',
        type: mType,
        description: attachments.first['description'] ?? '',
        typeFile: typeFile,
        url: urlFile,
        image: image);
  }
}

class ImageMessage {
  String url;
  double height;
  double width;
  ImageMessage({
    required this.url,
    required this.height,
    required this.width,
  });

  factory ImageMessage.fromMap(Map<String, dynamic> map) {
    String mUrl = RocketWebSocket.serverUrl + map['title_link'];
    return ImageMessage(
      url: mUrl,
      height: map['image_dimensions']['height'].toDouble() ?? 0.0,
      width: map['image_dimensions']['width'].toDouble() ?? 0.0,
    );
  }
}
