class LoginResponse {
  String message;
  LoginData? loginData;
  LoginResponse({
    required this.message,
    this.loginData,
  });

  Map<String, dynamic> toMap() {
    return {
      'status': message,
      'loginData': loginData?.toMap(),
    };
  }

  factory LoginResponse.fromMap(Map<String, dynamic> map) {
    return LoginResponse(
      message: map['status'] ?? '',
      loginData: map['data'] != null ? LoginData.fromMap(map['data']) : null,
    );
  }
}

class LoginData {
  String userId;
  String authToken;
  String status;
  String userName;
  String name;
  String avatarUrl;
  bool isActive;
  LoginData({
    required this.userId,
    required this.authToken,
    required this.status,
    required this.userName,
    required this.avatarUrl,
    this.name = '',
    required this.isActive,
  });

  Map<String, dynamic> toMap() {
    return {
      'userId': userId,
      'authToken': authToken,
      'status': status,
      'userName': userName,
      'avatarUrl': avatarUrl,
      'isActive': isActive,
    };
  }

  factory LoginData.fromMap(Map<String, dynamic> map) {
    return LoginData(
      userId: map['userId'] ?? '',
      authToken: map['authToken'] ?? '',
      status: map['status'] ?? '',
      name: map['me']['name'] ?? '',
      userName: map['me']['username'] ?? '',
      avatarUrl: map['avatarUrl'] ?? '',
      isActive: map['active'] ?? false,
    );
  }
}
