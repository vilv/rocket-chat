class User {
  User({
    required this.id,
    required this.status,
    required this.active,
    required this.name,
    required this.username,
    required this.nameInsensitive,
  });

  String id;
  String status;
  bool active;
  String name;
  String username;
  String nameInsensitive;

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["_id"],
        status: json["status"],
        active: json["active"],
        name: json["name"],
        username: json["username"],
        nameInsensitive: json["nameInsensitive"],
      );
}
