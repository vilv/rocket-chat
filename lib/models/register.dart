class RegisterReq {
  String userName;
  String email;
  String name;
  String password;
  RegisterReq({
    required this.userName,
    required this.email,
    required this.name,
    required this.password,
  });

  Map<String, dynamic> toMap() {
    return {
      'username': userName,
      'email': email,
      'name': name,
      'pass': password,
    };
  }
}
