import 'package:rocket_chat_connector_flutter/models/message.dart';

class ChannelRocket {
  ChannelRocket({
    required this.id,
    // required this.fname,
    // required this.customFields,
    // required this.description,
    // required this.broadcast,
    // required this.encrypted,
    required this.name,
    // required this.t,
    // required this.msgs,
    // required this.usersCount,
    // required this.u,
    // required this.ts,
    // required this.ro,
    // required this.welcomeDefault,
    // required this.sysMes,
    // required this.updatedAt,
    // required this.lastMessage,
    // required this.lm,
  });

  String id;
  // String fname;
  // CustomFields customFields;
  // String description;
  // bool broadcast;
  // bool encrypted;
  String name;
  // String t;
  // int msgs;
  // int usersCount;
  // WelcomeU u;
  // DateTime ts;
  // bool ro;
  // bool welcomeDefault;
  // bool sysMes;
  // DateTime updatedAt;
  // LastMessage lastMessage;
  // DateTime lm;

  factory ChannelRocket.fromJson(Map<String, dynamic> json) => ChannelRocket(
        id: json["_id"],
        // fname: json["fname"],
        // customFields: CustomFields.fromJson(json["customFields"]),
        // description: json["description"],
        // broadcast: json["broadcast"],
        // encrypted: json["encrypted"],
        name: json["name"],
        // t: json["t"],
        // msgs: json["msgs"],
        // usersCount: json["usersCount"],
        // u: WelcomeU.fromJson(json["u"]),
        // ts: DateTime.parse(json["ts"]),
        // ro: json["ro"],
        // welcomeDefault: json["default"],
        // sysMes: json["sysMes"],
        // updatedAt: DateTime.parse(json["_updatedAt"]),
        // lastMessage: LastMessage.fromJson(json["lastMessage"]),
        // lm: DateTime.parse(json["lm"]),
      );
}

class CustomFields {
  CustomFields();

  factory CustomFields.fromJson(Map<String, dynamic> json) => CustomFields();

  Map<String, dynamic> toJson() => {};
}

class LastMessage {
  LastMessage({
    required this.rid,
    required this.msg,
    required this.ts,
    required this.u,
    required this.id,
    required this.updatedAt,
    required this.urls,
    required this.mentions,
    required this.channels,
    required this.md,
  });

  String rid;
  String msg;
  DateTime ts;
  LastMessageU u;
  String id;
  DateTime updatedAt;
  List<dynamic> urls;
  List<dynamic> mentions;
  List<dynamic> channels;
  List<Md> md;

  factory LastMessage.fromJson(Map<String, dynamic> json) => LastMessage(
        rid: json["rid"],
        msg: json["msg"],
        ts: DateTime.parse(json["ts"]),
        u: LastMessageU.fromJson(json["u"]),
        id: json["_id"],
        updatedAt: DateTime.parse(json["_updatedAt"]),
        urls: List<dynamic>.from(json["urls"].map((x) => x)),
        mentions: List<dynamic>.from(json["mentions"].map((x) => x)),
        channels: List<dynamic>.from(json["channels"].map((x) => x)),
        md: List<Md>.from(json["md"].map((x) => Md.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "rid": rid,
        "msg": msg,
        "ts": ts.toIso8601String(),
        "u": u.toJson(),
        "_id": id,
        "_updatedAt": updatedAt.toIso8601String(),
        "urls": List<dynamic>.from(urls.map((x) => x)),
        "mentions": List<dynamic>.from(mentions.map((x) => x)),
        "channels": List<dynamic>.from(channels.map((x) => x)),
        "md": List<dynamic>.from(md.map((x) => x.toJson())),
      };
}

class Md {
  Md({
    required this.type,
    required this.value,
  });

  String type;
  List<Value> value;

  factory Md.fromJson(Map<String, dynamic> json) => Md(
        type: json["type"],
        value: List<Value>.from(json["value"].map((x) => Value.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "value": List<dynamic>.from(value.map((x) => x.toJson())),
      };
}
class LastMessageU {
  LastMessageU({
    required this.id,
    required this.username,
    required this.name,
  });

  String id;
  String username;
  String name;

  factory LastMessageU.fromJson(Map<String, dynamic> json) => LastMessageU(
        id: json["_id"],
        username: json["username"],
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "username": username,
        "name": name,
      };
}

class WelcomeU {
  WelcomeU({
    required this.id,
    required this.username,
  });

  String id;
  String username;

  factory WelcomeU.fromJson(Map<String, dynamic> json) => WelcomeU(
        id: json["_id"],
        username: json["username"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "username": username,
      };
}
